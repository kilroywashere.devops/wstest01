package devops.kilroywashere.wstest01.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Item {
    private int id;
    private String libelle;

    public Item(int id, String libelle) {
        this.id = id;
        this.libelle = libelle;
    }
}
