package devops.kilroywashere.wstest01.controllers;

import devops.kilroywashere.wstest01.models.Item;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ItemsController {

    private final List<Item> items = new ArrayList<>();
    @Value("${wstest01.environment}")
    private String environment;
    @Value("${spring.datasource.url}")
    private String dbUrl;

    public ItemsController() {
        for (int i=1; i < 6; i++) {
            items.add(new Item(i, String.format("Item%d", i)));
        }
    }

    @GetMapping("/items")
    public Iterable<Item> getItems() {
        return items;
    }

    @GetMapping("/envcheck")
    public String getEnvironment() {
      return String.format("DB URL=%s\n", dbUrl) +
             String.format("Environment=%s\n", environment);
    }
}
